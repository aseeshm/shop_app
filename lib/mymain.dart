import 'package:flutter/material.dart';
import 'package:shop_app/screens/product_overview.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'my shop',
        theme: ThemeData(
          primarySwatch: Colors.purple,
          accentColor: Colors.deepOrange,
          fontFamily:'Lato',
        ),
        home: ProductsOverviewScreen());
  }
}
